// create
export const state = () => ({
  navIconOpen: false,
  bodyWidth: 0
})

export const mutations = {
  switchNavIcon(state) {
    state.navIconOpen = !state.navIconOpen
  },
  updateBodyWidth(state) {
    state.bodyWidth = document.body.offsetWidth
  }
}
