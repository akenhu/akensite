export default [
  { name: '首頁', path: '/' },
  { name: '關於我', path: '/about' },
  { name: '技能樹', path: '/skill' },
  { name: '開發經驗', path: '/portfolio' },
  { name: '聯絡我', path: '/contact' }
  // { name: '我的履歷', path: '/resume' }
]
