const projectNorm = {
  roles: '擔任角色',
  skills: '技術應用',
  tools: '工具應用',
  framework: '框架應用',
  APIs: 'API 應用',
  date: '專案日期',
  envs: '專案環境'
}

const projectList = [
  {
    title: '虛擬貨幣交易所',
    // link: 'https://www.bibigoex.com/',
    imgs: [
      { src: '/img/portfolio-min/bibigo/1.jpg', alt: '' },
      { src: '/img/portfolio-min/bibigo/2.jpg', alt: '' }
    ],
    description: '虛擬貨幣交易所平台',
    date: ['2020/11', 'Now'],
    contents: {
      roles: ['Frontend'],
      skills: ['React.js', 'Ant Design', 'SCSS', 'React Hook', 'i18n', 'Apollo GraphQL'],
      tools: ['VS Code', 'GitLab'],
      framework: [],
      APIs: [],
      envs: ['GCP', 'Docker', 'Nginx']
    }
  },
  {
    title: '鷹馬急速瀏覽器 - 形象網站',
    // link: 'https://ttyzzcy.com/',
    imgs: [{ src: '/img/portfolio-min/bwr/1.jpg', alt: '' }],
    description: '鷹馬急速瀏覽器官網(前/後台)',
    date: ['2020/5', '2020/7'],
    contents: {
      roles: ['Frontend'],
      skills: ['Nuxt.js', 'Ant Design Vue', 'SCSS', 'Pug'],
      tools: ['VS Code', 'GitLab'],
      framework: [],
      APIs: [],
      envs: ['GCP', 'Nginx']
    }
  },
  {
    title: 'Ant Design Vue - 後台 UI',
    // link: 'https://www.bibigoex.com/',
    imgs: [
      { src: '/img/portfolio-min/antdv/antdv1.jpg', alt: '' },
      { src: '/img/portfolio-min/antdv/antdv2.jpg', alt: '' },
      { src: '/img/portfolio-min/antdv/antdv3.jpg', alt: '' }
    ],
    description: 'Ant Design Vue - 後台 UI Demo',
    date: ['2020/11', 'Now'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'Ant Design Vue', 'SCSS', 'i18n'],
      tools: ['VS Code', 'GitLab'],
      framework: [],
      APIs: [],
      envs: ['Docker', 'Nginx']
    }
  },
  {
    title: 'DMP',
    imgs: [
      { src: '/img/portfolio-min/dmp/4.jpg', alt: '' },
      { src: '/img/portfolio-min/dmp/2.jpg', alt: '' },
      { src: '/img/portfolio-min/dmp/3.jpg', alt: '' }
    ],
    description: '邁圈數位整合產品 - 追蹤碼資料管理平台。',
    date: ['2019/04', '2020/04'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'Lazy4-vue', 'SCSS', 'JS', 'Node.js', 'GA', 'FB Pixel', 'Google Ads'],
      tools: ['VS Code', 'MAMP', 'Navicat Premium', 'Xmind', 'GitLab', 'Redmine', 'Postman', 'apiDocs', 'GTM'],
      framework: [],
      APIs: [],
      envs: ['GCP', 'Ubuntu 18.04', 'PostgreSQL', 'MongoDB', 'Nginx', 'Docker', 'CloudFlare']
    }
  },
  {
    title: '華為新影像活動網站',
    imgs: [
      { src: '/img/portfolio-min/huaweifans/1.jpg', alt: '' },
      { src: '/img/portfolio-min/huaweifans/2.jpg', alt: '' },
      { src: '/img/portfolio-min/huaweifans/3.jpg', alt: '' }
    ],
    link: 'https://www.huaweifans.com.tw/nextimage2019',
    description: '邁圈數位整合外包案 - 華為新影像 2019 攝影大賽。',
    date: ['2019/04', '2019/04'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'SCSS', 'PHP', 'Lazy4-vue'],
      tools: ['VS Code', 'GitLab', 'JQuery'],
      framework: [],
      APIs: [],
      envs: ['MariaDB', 'Ubuntu', 'Nginx']
    }
  },
  {
    title: '二級預防及健康管理 App API',
    imgs: [
      { src: '/img/portfolio-min/spEcardiac/1.jpg', alt: '' },
      { src: '/img/portfolio-min/spEcardiac/2.jpg', alt: '' },
      { src: '/img/portfolio-min/spEcardiac/4.jpg', alt: '' }
    ],
    link: 'https://www.hxesther.com/#lan=tw&web=coreray.html',
    description: '同心測寶，與顧問管理平台結合，並可獲得醫師醫囑。',
    date: ['2018/06', '2019/03'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'LESS', 'JS', 'PHP', 'Canvas', 'APNs', 'Baidu Push API', 'WeChat API', 'Aliyun Mail API'],
      tools: ['VS Code', 'Bitbucket', 'XAMPP', 'Draw.io', 'Trello', 'Cloud 9'],
      framework: [],
      APIs: [],
      envs: ['Alibaba Cloud', 'AWS', 'Ubuntu 14.04', 'Nginx', 'MySQL']
    }
  },
  {
    title: '以斯帖客服管理平台',
    imgs: [
      { src: '/img/portfolio-min/hxestherService/1.jpg', alt: '' },
      { src: '/img/portfolio-min/hxestherService/2.jpg', alt: '' },
      { src: '/img/portfolio-min/hxestherService/3.jpg', alt: '' }
    ],
    description: '由客服人員使用，配合二級預防、心測寶，協助會員註冊、追蹤使用動態、列印測量報告。',
    date: ['2018/01', '2019/03'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'LESS', 'JS', 'Node.js', 'Canvas'],
      tools: ['VS Code', 'Bitbucket', 'XAMPP', 'Trello', 'PHPMyAdmin'],
      framework: [],
      APIs: [],
      envs: ['Alibaba Cloud', 'Ubuntu 14.04', 'Nginx', 'MySQL']
    }
  },
  {
    title: '河南華夏以斯帖官方網站',
    imgs: [
      { src: '/img/portfolio-min/hxesther/1.jpg', alt: '' },
      { src: '/img/portfolio-min/hxesther/2.jpg', alt: '' },
      { src: '/img/portfolio-min/hxesther/3.jpg', alt: '' }
    ],
    link: 'https://www.hxesther.com/#lan=tw&web=coreray.html',
    description: '科睿生物科技分公司 - 公司形象網站。',
    date: ['2017/01', '2019/03'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'LESS', 'JS', 'i18n'],
      tools: ['VS Code', 'Bitbucket'],
      framework: [],
      APIs: [],
      envs: ['Alibaba Cloud', 'Ubuntu 14.04', 'Nginx']
    }
  },
  {
    title: '醫師顧問管理平台',
    imgs: [
      { src: '/img/portfolio-min/hxesther-doc/1.jpg', alt: '' },
      { src: '/img/portfolio-min/hxesther-doc/2.jpg', alt: '' },
      { src: '/img/portfolio-min/hxesther-doc/4.jpg', alt: '' }
    ],
    description: '結合二級預防及心測寶 App 與河南省正骨醫院臨床醫師合作，追蹤病患檢測狀態並下達醫囑。',
    date: ['2016/09', '2019/03'],
    contents: {
      roles: ['Frontend', 'Backend', 'UI'],
      skills: ['HTML', 'LESS', 'JS', 'Webpack', 'Vue.js', 'PHP', 'Canvas', 'SVG', 'JQuery'],
      tools: ['VS Code', 'Bitbucket', 'XAMPP', 'Draw.io', 'Trello', 'Cloud 9', 'PHPMyAdmin'],
      framework: [],
      APIs: [],
      envs: ['Alibaba Cloud', 'Ubuntu 14.04', 'Nginx', 'MySQL']
    }
  },
  {
    title: '心測寶 App',
    imgs: [
      { src: '/img/portfolio-min/ecardiac/2.jpg', alt: '' },
      { src: '/img/portfolio-min/ecardiac/3.jpg', alt: '' },
      { src: '/img/portfolio-min/ecardiac/4.jpg', alt: '' }
    ],
    link: 'https://www.hxesther.com/#lan=tw&web=health_cloud.html',
    description: '透過 Truly 血壓計測量，進行數據分析，判斷心血管疾病與血壓報告。',
    date: ['2016/02', '2019/03'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'LESS', 'JS', 'PHP', 'Canvas', 'APNs', 'Baidu Push API', 'WeChat API', 'Aliyun Mail API'],
      tools: ['VS Code', 'Bitbucket', 'XAMPP', 'Draw.io', 'Trello', 'Cloud 9', 'PHPMyAdmin'],
      framework: [],
      APIs: [],
      envs: ['Alibaba Cloud', 'AWS', 'Ubuntu 14.04', 'Nginx', 'MySQL']
    }
  },
  {
    title: '兆豐文創有限公司官方網站',
    imgs: [
      { src: '/img/portfolio-min/harvesteducation/1.jpg', alt: '' },
      { src: '/img/portfolio-min/harvesteducation/2.jpg', alt: '' },
      { src: '/img/portfolio-min/harvesteducation/3.jpg', alt: '' }
    ],
    link: 'http://harvesteducation.cc',
    description: '大寶科技分公司 - 兆豐文創形象網站。',
    date: ['2013/09', '2013/10'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'CSS', 'JS'],
      tools: ['Dreamweaver'],
      framework: [],
      APIs: [],
      envs: ['Windows server 2003', 'Apache']
    }
  },
  {
    title: '21 天腦波雲端教室',
    imgs: [
      { src: '/img/portfolio-min/brainwave-class/1.jpg', alt: '' },
      { src: '/img/portfolio-min/brainwave-class/2.jpg', alt: '' }
    ],
    link: 'http://www.harvesteducation.cc/login/index.html',
    description: `與神念腦波耳機藍牙連結，透過模擬書法寫字練習，
                  訓練與分析使用者大腦的專注與放鬆，目的為解決專注力缺乏症以及無法幫助自我放鬆的訓練軟體，並有網站顯示測量報告。`,
    date: ['2013/02', '2014/01'],
    contents: {
      roles: ['Frontend', 'Backend'],
      skills: ['HTML', 'CSS', 'JS', 'PHP', 'ActionScript3.0'],
      tools: ['Dreamweaver', 'PHPMyAdmin', 'Adobe Flash Professional'],
      framework: [],
      APIs: [],
      envs: ['Windows server 2003', 'MySQL', 'Apache']
    }
  }
]

const caseList = [
  {
    title: '顥玥數位科技官方網站',
    imgs: [
      { src: '/img/portfolio-min/hao-yue/1.jpg', alt: '' },
      { src: '/img/portfolio-min/hao-yue/2.jpg', alt: '' },
      { src: '/img/portfolio-min/hao-yue/3.jpg', alt: '' }
    ],
    link: 'https://demo.aken.life/hao-yue-guan-wang',
    description: '個人團隊外包案 - 公司形象網站。',
    date: ['2019/06'],
    contents: {
      roles: ['Frontend'],
      skills: ['HTML', 'LESS', 'JS'],
      tools: ['VS Code', 'Bitbucket', 'Adobe XD'],
      framework: [],
      APIs: [],
      envs: ['XAMPP']
    }
  },
  {
    title: '坂茂綠能工程網站管理後台',
    imgs: [
      { src: '/img/portfolio-min/banmao-backend/1.jpg', alt: '' },
      { src: '/img/portfolio-min/banmao-backend/2.jpg', alt: '' },
      { src: '/img/portfolio-min/banmao-backend/3.jpg', alt: '' }
    ],
    description: '個人團隊外包案 - 公司形象網站管理後台。',
    date: ['2018/12', '2019/04'],
    contents: {
      roles: ['PM', 'Frontend'],
      skills: ['HTML', 'SCSS', 'JS', 'Node.js'],
      tools: ['VS Code', 'Bitbucket', 'Trello'],
      APIs: [],
      envs: ['ConoHa', 'Ubuntu 18.04', 'Nginx', 'MariaDB', 'CloudFlare', 'Docker']
    }
  },
  {
    title: '坂茂綠能工程官方網站',
    imgs: [
      { src: '/img/portfolio-min/banmao/1.jpg', alt: '' },
      { src: '/img/portfolio-min/banmao/2.jpg', alt: '' },
      { src: '/img/portfolio-min/banmao/3.jpg', alt: '' }
    ],
    link: 'https://banmao.com.tw',
    description: '個人團隊外包案 - 公司形象網站。',
    date: ['2018/12', '2019/04'],
    contents: {
      roles: ['PM', 'Frontend'],
      skills: ['HTML', 'SCSS', 'JS', 'Node.js'],
      tools: ['VS Code', 'Bitbucket', 'LINE bot', 'Adobe XD', 'Trello'],
      APIs: [],
      envs: ['ConoHa', 'Ubuntu 18.04', 'Nginx', 'MariaDB', 'CloudFlare', 'Docker']
    }
  },
  {
    title: '高雄市空間藝術學會網站管理後台',
    imgs: [
      { src: '/img/portfolio-min/kaisd-backend/1.jpg', alt: '' },
      { src: '/img/portfolio-min/kaisd-backend/2.jpg', alt: '' },
      { src: '/img/portfolio-min/kaisd-backend/3.jpg', alt: '' }
    ],
    link: '',
    description: '個人團隊外包案 - 學會形象網站管理後台。',
    date: ['2016/11', '2017/02 '],
    contents: {
      roles: ['PM', 'Frontend'],
      skills: ['HTML', 'LESS', 'JS', 'PHP'],
      tools: ['Sublime Text', 'Bitbucket', 'PHPMyAdmin'],
      APIs: [],
      envs: ['ConoHa', 'Ubuntu 18.04', 'Nginx', 'MariaDB', 'CloudFlare', 'Docker']
    }
  },
  {
    title: '高雄市空間藝術學會官方網站',
    imgs: [
      { src: '/img/portfolio-min/kaisd/1.jpg', alt: '' },
      { src: '/img/portfolio-min/kaisd/2.jpg', alt: '' },
      { src: '/img/portfolio-min/kaisd/3.jpg', alt: '' }
    ],
    link: 'https://www.kaisd.com.tw',
    description: '個人團隊外包案 - 學會形象網站。',
    date: ['2016/11', '2017/02 '],
    contents: {
      roles: ['PM', 'Frontend'],
      skills: ['HTML', 'LESS', 'JS', 'PHP'],
      tools: ['Sublime Text', 'Bitbucket', 'PHPMyAdmin'],
      APIs: [],
      envs: ['ConoHa', 'Ubuntu 18.04', 'Nginx', 'MariaDB', 'CloudFlare', 'Docker']
    }
  }
]

const sideProjectList = [
  {
    title: '台灣屏東 - 台電電線桿地圖',
    imgs: [
      { src: '/img/portfolio-min/taipower/1.png', alt: '' },
      { src: '/img/portfolio-min/taipower/2.png', alt: '' }
    ],
    link: 'https://taipower.aken.life/',
    description: `幫朋友做的小專案，方便他們用編號直接找到電線桿，並開啟 Google Map 導航。技術上是先用爬蟲去抓開放的【屏東電桿坐標及桿號】，所有桿號和座標寫到資料庫，讓使用者搜尋後，再用爬蟲去爬【台電圖號座標定位系統】，利用 TWD67 座標取得經緯度後，再連結到 google map。`,
    date: ['2021/03'],
    contents: {
      roles: ['FullStack'],
      skills: ['Vue.js', 'Element UI', 'Node.js'],
      framework: [],
      APIs: [],
      envs: ['GitLab Pages']
    }
  },
  {
    title: 'Bootstrap-Vue UI Library',
    imgs: [
      { src: '/img/portfolio-min/febv/1.jpg', alt: '' },
      { src: '/img/portfolio-min/febv/2.jpg', alt: '' }
    ],
    link: 'https://www.npmjs.com/package/@igold/fe-bootstrap-vue',
    description: 'Bootstrap-Vue 的 UI Library，幫助前端快速開發，未來可按需加載各種頁面、組件、工具和 Mock Server(開發中)。',
    date: ['2021/02', 'Now'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'Bootstrap-Vue'],
      framework: [],
      APIs: [],
      envs: ['GitLab Pages']
    }
  },
  {
    title: 'XFL 旅遊筆記',
    imgs: [
      { src: '/img/portfolio-min/xfl-note/1.jpg', alt: '' },
      { src: '/img/portfolio-min/xfl-note/2.jpg', alt: '' },
      { src: '/img/portfolio-min/xfl-note/3.jpg', alt: '' },
      { src: '/img/portfolio-min/xfl-note/4.jpg', alt: '' }
    ],
    link: 'https://xflnote.aken.life',
    description:
      '這是參考 HackMD 的功能，設計的筆記本，主要是為了讓朋友方便看旅遊內容，因為 Trello 無法設計 Style，用手機看得很痛苦，才會設計這套網站，來同步 Trello 的列表和卡片(現在都用 Notion 了，所以沒有在維護了)。',
    date: ['2020/12'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'Nuxt.js', 'Express', 'Node.js', 'markdown-it', 'CodeMirror'],
      framework: [],
      APIs: ['Trello API'],
      envs: []
    }
  },
  {
    title: '時間管理計時器',
    imgs: [
      { src: '/img/portfolio-min/timer/1.jpg', alt: '' },
      { src: '/img/portfolio-min/timer/2.jpg', alt: '' },
      { src: '/img/portfolio-min/timer/3.jpg', alt: '' }
    ],
    link: 'https://timer.aken.life',
    description:
      '在工作上，可以觀察自己都把時間浪費在甚麼地方上，而設計的時間管理器，可以透過點擊時鐘下方的文字開始計時，點擊其他類型可切換時間，還可以匯出 excel，方便你自己統計。',
    date: ['2020/02'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js'],
      framework: [],
      APIs: [],
      envs: []
    }
  },
  {
    title: 'Lazy4-vue UI Library',
    imgs: [
      { src: '/img/portfolio-min/lazy4vue/1.jpg', alt: '' },
      { src: '/img/portfolio-min/lazy4vue/2.jpg', alt: '' },
      { src: '/img/portfolio-min/lazy4vue/3.jpg', alt: '' }
    ],
    link: 'http://demo-lazy4vue.aken.life/',
    description: '於邁圈數位公司應用，結合 Kule Lazy 4 Vue Framework 的 UI Library，幫助前端快速開發(已暫停)。',
    date: ['2020/01', '2020/04'],
    contents: {
      roles: ['Frontend'],
      skills: ['Vue.js', 'Kule Lazy 4'],
      framework: [],
      APIs: [],
      envs: ['Ubuntu 18.08', 'Nginx']
    }
  }
]

export { projectNorm, projectList, caseList, sideProjectList }
