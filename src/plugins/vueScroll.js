import Vue from 'vue'
import vueScroll from 'vuescroll'
Vue.prototype.$vueScroll = vueScroll
// You can set global config here.
Vue.use(vueScroll, {
  ops: {
    mode: 'native',
    sizeStrategy: 'percent',
    detectResize: true,
    /** Enable locking to the main axis if user moves only slightly on one of them at start */
    locking: true,
    easing: 'easeInOutQuart',
    rail: {
      // background: '#01a99a',
      opacity: 0
      // size: '6px',
      // specifyBorderRadius: false,
      // gutterOfEnds: null,
      // gutterOfSide: '2px',
      // keepShow: false
    },
    scrollPanel: {
      // initialScrollY: false,
      // initialScrollX: false,
      // scrollingX: true,
      // scrollingY: true,
      // speed: 300,
      easing: 'easeInOutQuint'
      // verticalNativeBarPos: 'right'
    },
    bar: {
      // showDelay: 500,
      // onlyShowBarOnScroll: true,
      // keepShow: false,
      background: '#999',
      opacity: 0.8
      // hoverStyle: false，
      // specifyBorderRadius: false,
      // minSize: 0,
      // size: '6px',
      // disable: false
    }
  },
  name: 'vueScroll' // customize component name, default -> vueScroll
})
