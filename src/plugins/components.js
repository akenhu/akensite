import Vue from 'vue'

import section from '@/components/section'
import vueScroll from 'vuescroll'

Vue.component('Section', section)
Vue.component('VueScroll', vueScroll)
