export default {
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Aken Hu | 全端工程師 | 前端工程師 | 可獨立開發前/後台網站',
    htmlAttrs: {
      lang: 'zh'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Aken Hu | 我是全端工程師，也可以說我是前端工程師，目前正積極地專精於前端技能，我可以獨立開發前/後台專案，擅長用 Vue.js 開發，也有 React 經驗，若有需求或有意願合作的您，歡迎與我聯繫: aken.hu69@gmail.com / 0988340893。'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://cdn.jsdelivr.net/gh/kuletw/lazy@4.0.16b/dist/css/lazy4.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://pro.fontawesome.com/releases/v5.10.0/css/all.css',
        integrity: 'sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p',
        crossorigin: 'anonymous'
      }
    ],
    script: [
      {
        src: 'https://cdn.jsdelivr.net/gh/kuletw/lazy@4.0.16b/dist/js/kule.urbrowser.min.js'
      },
      {
        hid: 'htm',
        innerHTML: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MSWRCG6H');`,
        type: 'text/javascript',
        async: true
      }
    ],
    __dangerouslyDisableSanitizers: ['script']
  },
  loading: { color: '#fff' },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/style.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~/plugins/gtm-body.js', ssr: false }, '@/plugins/components.js', '@/plugins/vueScroll.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/google-analytics' // https://google-analytics.nuxtjs.org/setup/
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/sitemap'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  server: {
    port: 3001, // default: 3000
    host: 'localhost', // default: localhost,
    timing: false
  },
  // watch: [
  //   '@/store/*.js',
  //   '@/setting.config.js',
  //   // '@/components/*',
  //   '@/components/**/*'
  // ]
  srcDir: 'src/',
  alias: {
    '@UTILS': '@/assets/utils',
    '@CONFIG': '@/assets/config'
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'src/pages/error/404.vue')
      })
    },
    middleware: ['index']
  },
  sitemap: {
    hostname: 'https://aken.life',
    gzip: true, // 生成 .xml.gz 檔的 sitemap
    exclude: ['/secret', '/admin/**', '/img/**', '/files/**'],
    routes: [
      '/',
      '/about',
      '/contact',
      '/resume',
      {
        url: '/skill',
        changefreq: 'daily', // 可能變更的頻率
        priority: 1, // 網頁的重要程度，0.1 - 1
        lastmod: '2021-03-22T13:30:00.000Z'
      },
      {
        url: '/portfolio',
        changefreq: 'daily', // 可能變更的頻率
        priority: 1, // 網頁的重要程度，0.1 - 1
        lastmod: '2021-03-22T13:30:00.000Z'
      }
    ]
  },
  googleAnalytics: {
    id: process.env.GOOGLE_ANALYTICS_ID // Use as fallback if no runtime config is provided
  }
}
