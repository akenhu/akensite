module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: ['@nuxtjs', 'prettier', 'prettier/vue', 'plugin:prettier/recommended', 'plugin:nuxt/recommended'],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/no-unused-components': [
      'warn',
      {
        ignoreWhenBindingPresent: true
      }
    ],
    // 'nuxt/no-cjs-in-config': 'off',
    // 'vue/singleline-html-element-content-newline': 'off', // html 元素斷行; off:不限制; number: 根據長度決定要不要斷行; always: 強制斷行
    // 'vue/multiline-html-element-content-newline': 'off', //  html 元素斷行; off:不限制; number: 根據長度決定要不要斷行; always: 強制斷行
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'any',
          component: 'any'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    // 'vue/no-v-html': 0, // 0 = off, 1 = warn, 2 = error
    'prettier/prettier': [
      'error',
      {
        tabWidth: 2,
        singleQuote: true,
        printWidth: 140,
        semi: false,
        trailingComma: 'none',
        htmlWhitespaceSensitivity: 'ignore'
      }
    ]
  }
}
